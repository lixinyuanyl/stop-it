package com.cskaoyan.listener;

import com.cskaoyan.util.MyBatisUtil;
import com.cskaoyan.util.WdConstant;
import org.apache.ibatis.session.SqlSessionFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author stone
 * @date 2023/03/16 16:39
 */
@WebListener
public class MyBatisInitializationListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext servletContext = servletContextEvent.getServletContext();
        SqlSessionFactory sqlSessionFactory = MyBatisUtil.getSqlSessionFactory();
        servletContext.setAttribute(WdConstant.SQL_SESSION_FACTORY, sqlSessionFactory);

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
