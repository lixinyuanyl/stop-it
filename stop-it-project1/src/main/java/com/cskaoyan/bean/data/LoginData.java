package com.cskaoyan.bean.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author stone
 * @date 2022/12/28 09:38
 */
@NoArgsConstructor
@Data
public class LoginData {
    //>>>>>HEAD
    private List<String> roles;
    /*-----------------
    private String[] roles;
    <<<<<<<<<<a213e532673427abc*/
    private String name;
    private List<String> perms;
    private String avatar;
}
