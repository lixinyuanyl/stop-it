package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * @author stone
 * @date 2023/03/17 17:35
 */
@Data
public class RegionVo {
    private Integer id;
    private String name;
    private Byte type;
    private Integer code;

    private List<RegionVo> children;
}

