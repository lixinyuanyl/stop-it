package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @author stone
 * @date 2023/03/18 00:57
 */
@Data
public class UserVo {
    private String nickname;
    private String avatar;

}
