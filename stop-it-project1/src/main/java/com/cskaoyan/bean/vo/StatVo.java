package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author stone
 * @date 2023/03/20 16:13
 */
@Data
public class StatVo {
    private String[] columns = new String[0];
    private List<Map> rows = new ArrayList<>();
}
