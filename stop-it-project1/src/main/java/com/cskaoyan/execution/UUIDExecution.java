package com.cskaoyan.execution;

import java.util.UUID;

/**
 * @author stone
 * @date 2023/04/05 11:16
 */
public class UUIDExecution {

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {

            String uuid = UUID.randomUUID().toString();
            System.out.println(uuid);
        }
    }
}
