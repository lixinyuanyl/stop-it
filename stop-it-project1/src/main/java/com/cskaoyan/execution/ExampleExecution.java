package com.cskaoyan.execution;

import com.cskaoyan.bean.MarketGoods;
import com.cskaoyan.bean.MarketGoodsExample;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.util.MyBatisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author stone
 * @date 2023/04/05 14:46
 */
public class ExampleExecution {

    public static void main(String[] args) {
        //exampleDemo1();
        MarketGoodsMapper marketGoodsMapper = MyBatisUtil.getMapper(MarketGoodsMapper.class);
        // 查询全部记录
        // 第一页，每页20条记录
        //PageHelper.startPage(1, 20);
        // 第5页，每页20条记录
        PageHelper.startPage(5, 20);
        //List<MarketGoods> marketGoods = marketGoodsMapper.selectByExample(null);
        List<MarketGoods> marketGoods = marketGoodsMapper.selectByExample(new MarketGoodsExample());
        //long count = marketGoodsMapper.countByExample(new MarketGoodsExample());
        // pageNum\pageSize\total
        PageInfo<MarketGoods> pageInfo = new PageInfo<>(marketGoods);
        long total = pageInfo.getTotal();

    }

    private static void exampleDemo1() {
        MarketGoodsMapper marketGoodsMapper = MyBatisUtil.getMapper(MarketGoodsMapper.class);
        // select * from cskaoyan_goods
        // where goods_sn like '%1006%'
        // and name like '%帽子%'
        // and category_id = 101
        // and brand_id = 201
        MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
        // 创建一个空的条件 List<条件>
        MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
        criteria.andGoodsSnLike("%1006%")
                .andNameLike("%帽子%")
                .andCategoryIdEqualTo(101)
                .andBrandIdEqualTo(201);


        List<MarketGoods> marketGoods = marketGoodsMapper.selectByExample(marketGoodsExample);
    }
}
