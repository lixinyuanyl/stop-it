package com.cskaoyan.servlet;

import com.cskaoyan.util.DispatchUtil;
import com.cskaoyan.util.URIUtil;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author stone
 * @date 2023/03/20 11:45
 */
public class WdBaseServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handle(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        handle(req,resp);
    }

    //protected abstract void handle(String operation, HttpServletRequest request, HttpServletResponse response) throws IOException;
    @SneakyThrows
    protected void handle( HttpServletRequest request, HttpServletResponse response) throws IOException {

        DispatchUtil.dispatch(request,response,this);
    }
}
