package com.cskaoyan.servlet;

import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.UserService;
import com.cskaoyan.service.UserServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author stone
 * @date 2023/04/05 15:17
 */
@WebServlet("/admin/user/*")
public class AdminUserServlet extends WdBaseServlet {

    UserService userService = new UserServiceImpl();

    public BaseRespVo list(HttpServletRequest request, HttpServletResponse response) {
        Integer page = Integer.parseInt(request.getParameter("page"));
        Integer limit = Integer.parseInt(request.getParameter("limit"));
        String order = request.getParameter("order");
        String sort = request.getParameter("sort");
        String username = request.getParameter("username");

        CommonData data = userService.query(page,limit,sort,order,username);
        return BaseRespVo.ok(data);//会转换为json字符，然后通过 response的getWriter去响应
    }
}
