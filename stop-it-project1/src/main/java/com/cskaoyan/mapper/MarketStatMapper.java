package com.cskaoyan.mapper;

import java.util.List;
import java.util.Map;

/**
 * @author stone
 * @date 2023/03/20 16:15
 */
public interface MarketStatMapper {
    List<Map> statUser();
}
