package com.cskaoyan.service;

import com.cskaoyan.bean.common.CommonData;

/**
 * @author stone
 * @date 2023/04/05 15:26
 */
public interface UserService {
    CommonData query(Integer page, Integer limit, String sort, String order, String username);
}
