package com.cskaoyan.service;

import com.cskaoyan.bean.MarketUser;
import com.cskaoyan.bean.MarketUserExample;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.util.MyBatisUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * @author stone
 * @date 2023/04/05 15:27
 */
public class UserServiceImpl implements UserService {
    @Override
    public CommonData query(Integer page, Integer limit, String sort, String order, String username) {
        MarketUserMapper marketUserMapper = MyBatisUtil.getMapper(MarketUserMapper.class);
        // 1开启分页
        PageHelper.startPage(page, limit);

        // 2执行查询
        // a.构造Example对象，这个对象就是条件的容器
        MarketUserExample example = new MarketUserExample();
        // b.向这个容器里添加条件
        //    username like ?
        //    password = ?
        //    age = ?
        //    stats in (?,?,?)
        // 构造了一个空的条件的列表 → 目的是向这个列表里添加条件
        MarketUserExample.Criteria criteria = example.createCriteria();//List<条件>
        criteria.andUsernameLike("%" + username + "%");
                /*.andPasswordEqualTo("123456")
                .andGenderEqualTo((byte) 1);*/

        example.setOrderByClause(sort + " " + order);
        List<MarketUser> users = marketUserMapper.selectByExample(example);

        // 3获得分页信息并封装
        PageInfo<MarketUser> pageInfo = new PageInfo<>(users);
        /*CommonData data = new CommonData();
        data.setTotal((int) pageInfo.getTotal());
        data.setPages(pageInfo.getPages());
        data.setPage(pageInfo.getPageNum());
        data.setLimit(pageInfo.getPageSize());
        data.setList(pageInfo.getList());
        return data;*/
        CommonData data = CommonData.data(pageInfo);
        return data;
    }
}
