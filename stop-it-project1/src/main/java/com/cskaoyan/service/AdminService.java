package com.cskaoyan.service;

import com.cskaoyan.bean.MarketAdmin;

/**
 * @author stone
 * @date 2023/03/17 10:32
 */
public interface AdminService {
    MarketAdmin query(String username, String password);
}
