package com.cskaoyan.util;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * @author stone
 * @date 2023/03/16 17:57
 */
public class URIUtil {

    public static String getOperation(Class<?> servletClass,HttpServletRequest request) {
        String uri = request.getRequestURI();
        String contextPath = request.getContextPath();
        String appUri = uri.replaceAll(contextPath, "");
        WebServlet webServlet = servletClass.getDeclaredAnnotation(WebServlet.class);
        String urlPattern = webServlet.value()[0];
        String operation = appUri.substring(urlPattern.length() - 2);
        return operation;
    }
}
