package com.cskaoyan.util;

/**
 * @author stone
 * @date 2023/03/15 10:10
 */
public class WdConstant {

    public static final String SQL_SESSION_FACTORY = "sqlSessionFactory";
    public static final String MARKET_TOKEN = "X-CskaoyanMarket-Admin-Token";
    public static final String DEFAULT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

}
